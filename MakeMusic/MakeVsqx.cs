﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;

namespace MakeMusic
{
    public class MakeVsqx
    {
        static string outputpath = "output/";

        static int barMax = 64;

        public static void Make(string outputVsqxName)
        {
            string filePath = outputpath + outputVsqxName + ".vsqx";

            System.Console.WriteLine("Make VSQX FilePath: " + filePath);

            if (Directory.Exists(Path.GetDirectoryName(filePath)) == false)
            {
                Directory.CreateDirectory(Path.GetDirectoryName(filePath));
            }

            if (File.Exists(filePath))
            {
                File.Delete(filePath);
            }

            StreamWriter sw;
            FileInfo fi;
            fi = new FileInfo(filePath);
            sw = fi.AppendText();

            WiteHeader(sw);
            float newPitch = 0;
            float pitch = 0;
            //for(int tick = 0;tick<480*4*barMax;tick++){
            //    if(tick%(480/Random.Range(1,5)) == 0){
            //        newPitch = Random.Range(-32760,32760);	
            //    }

            //    if(tick%4 == 0){
            //        WritePitch(sw,tick,(int)(Mathf.Cos(tick/480f*8f) * 3000f) + 
            //                       (int)Mathf.Lerp(pitch ,newPitch+ (Mathf.Cos(tick/480f*2f) * 3000) ,(tick%480)/480f) );


            //        float dyn = Mathf.Cos(tick/480f*16f) * Mathf.Cos(tick/480f*3f);
            //        WriteDynamics( sw, tick,(int)(64 + (dyn*32.0f)));
            //    }


            //}

            WriteDynamics(sw, 0, 127);

            //randomNoteGen1(sw);

            timingToRandomGen(sw);

            WiteFooter(sw);

            sw.Flush();
            sw.Close();
        }

        /// <summary>
        /// 歌詞タイミング情報から生成
        /// </summary>
        /// <param name="sw"></param>
        static void timingToRandomGen(StreamWriter sw)
        {
            System.Random random = new System.Random();
            foreach (var timing in timingList)
            {
                int lastNote = 60;
                int note = (int)(12 + random.Next(54 - 6, 72 - 6) + Math.Sin(timing.start) * 12f);

                if (lastNote - note > 12)
                {
                    note += 12;
                }
                else if (note - lastNote > 12)
                {
                    note -= 12;
                }

                string lyric = timing.lyric;
                if (lyric == "")
                {
                    lyric = RandomNoteVoice();
                }

                //
                if (timing.note != 0)
                {
                    WriteNote(sw, ScaleNote(timing.note), timing.GetStartTick(), timing.GetDurTick(), lyric);
                }
                else
                {
                    WriteNote(sw, ScaleNote(note), timing.GetStartTick(), timing.GetDurTick(), lyric);
                }

                lastNote = note;

            }
        }

        /// <summary>
        /// ランダムに歌詞タイミング生成
        /// </summary>
        /// <param name="sw"></param>
        static void randomNoteGen1(StreamWriter sw)
        {

            int lastNote = 60;
            for (int tick = 0; tick < 480 * 4 * barMax; tick++)
            {
                System.Random random = new System.Random();

                int timing = tick % (4 * 480 / random.Next(1, 5));

                if (timing == 0 || timing == 480 / 2)
                {
                    int note = (int)(12 + random.Next(54 - 6, 72 - 6) + Math.Sin(tick) * 12f);

                    if (lastNote - note > 12)
                    {
                        note += 12;
                    }
                    else if (note - lastNote > 12)
                    {
                        note -= 12;
                    }

                    WriteNote(sw, ScaleNote(note), tick, (int)((480 * 4) / (random.Next(1, 5))), ScaleNoteVoice(note));
                    //WriteNote(sw, ScaleNote(72), tick, (int)((480 * 4) / (random.Next(1, 5))), ScaleNoteVoice(note));

                    lastNote = note;
                }
            }

        }

        static string ScaleNoteVoice(int note)
        {
            string ret = "ら";
            switch (note % 12)
            {
                case 0: ret = "さ"; break;
                case 1: ret = "さ"; break;
                case 2: ret = "れ"; break;
                case 3: ret = "れ"; break;
                case 4: ret = "が"; break;
                case 5: ret = "ま"; break;
                case 6: ret = "ま"; break;
                case 7: ret = "ぱ"; break;
                case 8: ret = "ぱ"; break;
                case 9: ret = "だ"; break;
                case 10: ret = "だ"; break;
                case 11: ret = "に"; break;
            }

            return ret;
        }

        static public string[] on50 = new string[] {
            "あ","い","う","え","お","か","き","く","け","こ","さ","し","す",
            "せ","そ","た","ち","つ","て","と","な","に","ぬ","ね","の",
            "は","ひ","ふ","へ","ほ","ま","み","む","め","も","や","ゆ","よ","わ","を","ん","ー"

        };

        static int lyricReadCount = 0;

        static string RandomNoteVoice()
        {
            string ret = "";
            if (lyricList.Count > 0)
            {
                ret = lyricList[lyricReadCount % (lyricList.Count - 1)];
                lyricReadCount++;
            }
            else
            {
                //  ランダム50音
                System.Random random = new System.Random();
                ret = on50[random.Next(0, on50.Length - 1)];
            }
            return ret;
        }


        static int ScaleNote(int note)
        {
            int ret = 0;
            switch (note % 12)
            {
                case 0: ret = 0; break;
                case 1: ret = 0; break;
                case 2: ret = 2; break;
                case 3: ret = 2; break;
                case 4: ret = 4; break;
                case 5: ret = 5; break;
                case 6: ret = 5; break;
                case 7: ret = 7; break;
                case 8: ret = 7; break;
                case 9: ret = 9; break;
                case 10: ret = 9; break;
                case 11: ret = 0; break;
            }

            return ret + ((note / 12) * 12);
        }

        static void WritePitch(StreamWriter sw, int tick, int pitch)
        {
            sw.WriteLine(@"<mCtrl>");
            sw.WriteLine(@"<posTick>" + tick.ToString() + "</posTick>");
            sw.WriteLine("<attr id=\"PIT\">" + pitch.ToString() + "</attr>");
            sw.WriteLine(@"</mCtrl>");
        }


        static void WriteDynamics(StreamWriter sw, int tick, int dyn)
        {
            sw.WriteLine(@"<mCtrl>");
            sw.WriteLine(@"<posTick>" + tick.ToString() + "</posTick>");
            sw.WriteLine("<attr id=\"DYN\">" + dyn.ToString() + "</attr>");
            sw.WriteLine(@"</mCtrl>");
        }

        static void WriteNote(StreamWriter sw, int note, int tick, int dur, string lyric)
        {
            sw.WriteLine(@"<note>");
            sw.WriteLine(@"	<posTick>" + tick.ToString() + "</posTick>");
            sw.WriteLine(@"	<durTick>" + dur.ToString() + "</durTick>");
            sw.WriteLine(@"	<noteNum>" + note.ToString() + "</noteNum>");
            sw.WriteLine(@"	<velocity>64</velocity>");
            sw.WriteLine(@"	<lyric><![CDATA[" + lyric + "]]></lyric>");
            //sw.WriteLine("	<phnms lock=\"1\"><![CDATA[4 i]]></phnms>");
            sw.WriteLine("	<phnms><![CDATA[4 i]]></phnms>");
            sw.WriteLine(@"	<noteStyle>");
            sw.WriteLine("		<attr id=\"accent\">50</attr>");
            sw.WriteLine("		<attr id=\"bendDep\">8</attr>");
            sw.WriteLine("		<attr id=\"bendLen\">0</attr>");
            sw.WriteLine("		<attr id=\"decay\">50</attr>");
            sw.WriteLine("		<attr id=\"fallPort\">0</attr>");
            sw.WriteLine("		<attr id=\"opening\">127</attr>");
            sw.WriteLine("		<attr id=\"risePort\">0</attr>");
            sw.WriteLine("		<attr id=\"vibLen\">0</attr>");
            sw.WriteLine("		<attr id=\"vibType\">1</attr>");
            sw.WriteLine(@"	</noteStyle>");
            sw.WriteLine(@"</note>");
        }


        static void WiteHeader(StreamWriter sw)
        {
            string path = "header.txt";
            if (System.IO.File.Exists(path) == false)
            {
                System.Console.WriteLine("Not found File : " + path);
                return;
            }
            FileStream file = new FileStream(path, FileMode.Open, FileAccess.Read);
            StreamReader reader = new StreamReader(file);
            string description = "", txtBuffer;
            while ((txtBuffer = reader.ReadLine()) != null)
            {
                description = description + txtBuffer + "\r\n";
            }
            sw.Write(description);
        }
        static void WiteFooter(StreamWriter sw)
        {
            string path = "footer.txt";
            if (System.IO.File.Exists(path) == false)
            {
                System.Console.WriteLine("Not found File : " + path);
                return;
            }
            FileStream file = new FileStream(path, FileMode.Open, FileAccess.Read);
            StreamReader reader = new StreamReader(file);
            string description = "", txtBuffer;
            while ((txtBuffer = reader.ReadLine()) != null)
            {
                description = description + txtBuffer + "\r\n";
            }
            sw.Write(description);
        }

        /// <summary>
        /// タイミングリスト
        /// </summary>
        public static List<Timing> timingList = new List<Timing>();

        public static void ReadTiming(string path)
        {
            timingList = new List<Timing>();

            // string path = "timing.csv";
            if (System.IO.File.Exists(path) == false)
            {
                System.Console.WriteLine("Not found File : " + path);
                return;
            }
            FileStream file = new FileStream(path, FileMode.Open, FileAccess.Read);
            StreamReader reader = new StreamReader(file);
            string description = "", txtBuffer;
            while ((txtBuffer = reader.ReadLine()) != null)  //  1行づつ読み込み
            {
                //description = description + txtBuffer + "\r\n";
                Timing timing = new Timing();
                timing.Text = txtBuffer;

                timingList.Add(timing);
            }
            System.Console.WriteLine(description);

        }

        public static List<string> lyricList = new List<string>();

        /// <summary>
        /// 歌詞ファイルがあれば読み込む
        /// </summary>
        /// <param name="p"></param>
        internal static void ReadLyric(string path)
        {
            lyricList = new List<string>();

            if (System.IO.File.Exists(path) == false)
            {
                System.Console.WriteLine("Not load lyric File : " + path);
                return;
            }

            FileStream file = new FileStream(path, FileMode.Open, FileAccess.Read);
            StreamReader reader = new StreamReader(file);
            string description = "", txtBuffer;
            while ((txtBuffer = reader.ReadLine()) != null)  //  1行づつ読み込み
            {
                string text = txtBuffer;
                for (int i = 0; i < text.Length; i++)
                {
                    lyricList.Add(text[i].ToString()); //  一文字づつ記録
                }
            }

            System.Console.WriteLine(description);
        }
    }
}
