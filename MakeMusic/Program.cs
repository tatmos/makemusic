﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MakeMusic
{
    class Program
    {
        static void Main(string[] args)
        {
            System.Console.WriteLine("Make Music!!");

            string fileName = "pokemik";
            if (args.Length > 0)
            {
                fileName = args[0];
            }

            MakeVsqx.ReadTiming(fileName + ".csv");
            MakeVsqx.ReadLyric(fileName + ".txt");

            MakeVsqx.Make(fileName);

            WaveManager.Read(); 

            WaveManager.Write(fileName + ".wav");

        }
    }
}
