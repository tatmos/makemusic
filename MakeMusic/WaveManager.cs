﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;


namespace MakeMusic
{
    class WaveManager
    {
        struct WavHeader
        {
            public byte[] riffID; // "riff"
            public uint size;  // ファイルサイズ-8
            public byte[] wavID;  // "WAVE"
            public byte[] fmtID;  // "fmt "
            public uint fmtSize; // fmtチャンクのバイト数
            public ushort format; // フォーマット
            public ushort channels; // チャンネル数
            public uint sampleRate; // サンプリングレート
            public uint bytePerSec; // データ速度
            public ushort blockSize; // ブロックサイズ
            public ushort bit;  // 量子化ビット数
            public byte[] dataID; // "data"
            public uint dataSize; // 波形データのバイト数
        }


        static WavHeader Header = new WavHeader();

        public static void Read()
        {
            string path = "tmp.wav";    //  テンポラリのwav

            Header = new WavHeader();
            List<short> lDataList = new List<short>();
            List<short> rDataList = new List<short>();

            #region 読み込み
            using (FileStream fs = new FileStream(path, FileMode.Open, FileAccess.Read))
            using (BinaryReader br = new BinaryReader(fs))
            {
                try
                {
                    Header.riffID = br.ReadBytes(4);
                    Header.size = br.ReadUInt32();
                    Header.wavID = br.ReadBytes(4);
                    Header.fmtID = br.ReadBytes(4);
                    Header.fmtSize = br.ReadUInt32();
                    Header.format = br.ReadUInt16();
                    Header.channels = br.ReadUInt16();
                    Header.sampleRate = br.ReadUInt32();
                    Header.bytePerSec = br.ReadUInt32();
                    Header.blockSize = br.ReadUInt16();
                    Header.bit = br.ReadUInt16();
                    Header.dataID = br.ReadBytes(4);
                    Header.dataSize = br.ReadUInt32();

                    for (int i = 0; i < Header.dataSize / Header.blockSize; i++)
                    {
                        lDataList.Add((short)br.ReadUInt16());
                        rDataList.Add((short)br.ReadUInt16());
                    }
                }
                finally
                {
                    if (br != null)
                    {
                        br.Close();
                    }
                    if (fs != null)
                    {
                        fs.Close();
                    }
                }
            }
            Console.WriteLine(path + "をオープンしました。");
            #endregion

        }




        /// <summary>
        /// 波形生成
        /// </summary>
        /// <param name="outputPath"></param>
        public static void Write(string _outputPath)
        {
            string filePath = "output/" + _outputPath;

            if (Directory.Exists(Path.GetDirectoryName(filePath)) == false)
            {
                Directory.CreateDirectory(Path.GetDirectoryName(filePath));
            }

            //string outputPath = "output.wav";   //  出力波形
            List<short> lDataList = new List<short>();
            List<short> rDataList = new List<short>();

            //  生成

            //for(int sample = 0;sample < 44100;sample++)
            {
                //lDataList.Add((short)((sample*100)*sample));
                //rDataList.Add((short)(sample*100));
            }
            System.Random random = new System.Random();

            int sample = 0;
            int count = 0;
            foreach (var timing in MakeVsqx.timingList)
            {
                int startSmaple = timing.GetStartSample();  //  開始位置を得る
                for (; sample < startSmaple; sample++)
                {
                    //  silent
                    lDataList.Add(0);
                    rDataList.Add(0);
                }

                int nextStartTime = MakeVsqx.timingList[count + 1].GetStartSample();    //  次の開始位置

                int rate = 441 * timing.note;
                if (rate == 0) rate = 441 * random .Next(32, 72);
                for (; sample < nextStartTime - (int)((double)44100 / (double)1000 * (double)200); sample++)
                {
                    int rest = nextStartTime - (int)((double)44100 / (double)1000 * (double)200) - sample;
                    //  sound
                    lDataList.Add((short)(((double)rate * (double)sample) % (65535)));
                    rDataList.Add((short)(((double)rate * (double)sample) % (65535)));
                }

                count++;
                if (count == MakeVsqx.timingList.Count - 1) break;
            }


            //  

            List<short> lNewDataList = lDataList;
            List<short> rNewDataList = rDataList;

            Header.dataSize = (uint)Math.Max(lNewDataList.Count, rNewDataList.Count) * 4;
            Header.size = (uint)Header.dataSize + 16 + 4;

            using (FileStream fs = new FileStream(filePath, FileMode.Create, FileAccess.Write))
            using (BinaryWriter bw = new BinaryWriter(fs))
            {
                try
                {
                    bw.Write(Header.riffID);
                    bw.Write(Header.size);
                    bw.Write(Header.wavID);
                    bw.Write(Header.fmtID);
                    bw.Write(Header.fmtSize);
                    bw.Write(Header.format);
                    bw.Write(Header.channels);
                    bw.Write(Header.sampleRate);
                    bw.Write(Header.bytePerSec);
                    bw.Write(Header.blockSize);
                    bw.Write(Header.bit);
                    bw.Write(Header.dataID);
                    bw.Write(Header.dataSize);

                    for (int i = 0; i < Header.dataSize / Header.blockSize; i++)
                    {
                        if (i < lNewDataList.Count)
                        {
                            bw.Write((ushort)lNewDataList[i]);
                        }
                        else
                        {
                            bw.Write(0);
                        }

                        if (i < rNewDataList.Count)
                        {
                            bw.Write((ushort)rNewDataList[i]);
                        }
                        else
                        {
                            bw.Write(0);
                        }
                    }
                }
                finally
                {
                    if (bw != null)
                    {
                        bw.Close();
                    }
                    if (fs != null)
                    {
                        fs.Close();
                    }
                }
            }

            return;
        }
    }
}
