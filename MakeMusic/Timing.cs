﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MakeMusic
{
    public class Timing
    {
        string text;

        public string Text
        {
            get { return text; }
            set
            {
                text = value;
                //  csv
                string[] split = text.Split(',');

                if (split.Length > 0)
                {
                    int outInt = 0;
                    if(int.TryParse(split[0],out outInt)){
                        start = outInt;
                        end = start + 200;
                    }
                }
                if (split.Length > 1)
                {
                    lyric = split[1];
                }
                if (split.Length > 2)
                {
                    int outInt = 0;
                    if (int.TryParse(split[2], out outInt))
                    {
                        end = outInt;
                    }
                } 
                if (split.Length > 3)
                {
                    int outInt = 0;
                    if (int.TryParse(split[3], out outInt))
                    {
                        note = outInt;
                    }
                }
            }
        }

        public int start = 0;           //  msec絶対時間
        public string lyric = "";
        public int end = 0;             //  msec絶対時間
        public int note = 0;

        //

        public int GetStartTick()
        {
            //  BPM120 4/4  480分解能  500msec

            return (int)((((double)480.0 / (double)500.0) * (double)start)); //  msec->tick
        }

        
        public int GetDurTick()
        {
            //  BPM120 4/4  480分解能  500msec

            return (int)((double)(((double)480.0 / (double)500.0) * (double)end)) - GetStartTick(); //  msec->tick
        }

        public int GetStartSample()
        {
            //  44100sample 1000msec
            return (int)(((double)44100 / (double)1000) * (double)start);
        }
    }
}
