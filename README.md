# README #

This README would normally document whatever steps are necessary to get your application up and running.

### What is this repository for? ###

* Quick summary
 http://hacklog.jp/works/3378 MusicHackDayTokyo2015　歌うドラム by ヒゲメガネ
で使われた変換ツール

* Version

* [Learn Markdown](https://bitbucket.org/tutorials/markdowndemo)

MusicHackDayTokyo2015 C#コンソール
タイミング CSVからVSQXとwavを作る

●MakeMusic.exeについて
タイミングファイルcsvからvsqxとwavファイルを生成します。
（Vocaloducerもどき）
cmd.exe上で
MakeMusic.exe [ファイル名（拡張子なし）]
を指定します。

【入力】
exeと同じフォルダ内の引数で指定した
ファイル名.csv（タイミングファイル）（必須）
ファイル名.txt(歌詞ファイル）（省略可能）
を解析します。
【出力】
output/ファイル名.vsqx 歌唱データ V3/VY1
output/ファイル名.wav 伴奏波形データ 
が生成されます。
●タイミングファイルのcsvのフォーマット
開始絶対時間msec,歌詞,終了絶対時間msec,MIDIノート番号
となっております。
歌詞　省略可能です。省略時ランダムまたは歌詞ファイルから割り当て
終了絶対時間msec　省略可能です。省略時固定長
MIDIノート番号　省略可能です。省略時ランダム

例えば、
1000,あ　←　1秒後に「あ」がランダムな音程で
1200,,1500　←　1秒後にランダムな歌詞でランダムな音程で1500でノートオフ
といった具合です。

既知の不具合：出力wavのサイズが不正な場合がある様子。

### How do I get set up? ###

* Summary of set up
* Configuration
* Dependencies
* Database configuration
* How to run tests
使うだけならReleseフォルダのmakemusic.exeが使えます。

* Deployment instructions

### Contribution guidelines ###

* Writing tests
* Code review
* Other guidelines

### Who do I talk to? ###

* Repo owner or admin
* Other community or team contact